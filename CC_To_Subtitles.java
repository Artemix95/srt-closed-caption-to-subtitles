import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CC_To_Subtitles {
    public static void main(String[] args) {

        String input;
        String inputCase;
        Scanner inCase = new Scanner(System.in);
        System.out.println("Sono da convertire in minuscolo? (s/n) ");
        inputCase = inCase.nextLine();
        switch (inputCase) {
            case "n" -> {
                do {

                    //input file from user
                    Scanner in = new Scanner(System.in);
                    System.out.println("What is the filename? ");
                    input = in.nextLine();

                    //check if there are quotation marks (start end), and removes it.
                    if ((input.charAt(0) == '"') && (input.charAt(input.length() - 1) == '"')) {
                        input = input.substring(1, input.length() - 1);
                    }
                    else if ((input.charAt(0) == '\'') && (input.charAt(input.length() - 2) == '\'') && (input.charAt(input.length()-1) == ' ')) {
                        input = input.substring(1, input.length() - 2);
                    }

                    //copy all lines of the file in a List "lines"
                    ArrayList<String> lines = new ArrayList<>();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(input), StandardCharsets.UTF_8));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            lines.add(line);
                        }
                    } catch (FileNotFoundException e) {
                        System.out.println("File not found");
                        continue;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    ArrayList<String> readyList;
                    readyList = freeSpaces(groupLines(removeEmptyLines(lines)));

                    ArrayList<String> finalList;
                    finalList = rimuoviTrattino2(rimuoviTrattino(eliminoParentesiQuadre(eliminoParentesiTonde(readyList))));

                    ArrayList<String> formatList;
                    formatList = formatStrings(adjustNewLines(finalList));


                    //Set new name for output converted file
                    String newFileName;
                    newFileName = input + "_cleared.srt";

                    //create a new file and check if the name already exists
                    File f = new File(newFileName);
                    if (f.exists() && !f.isDirectory()) {
                        System.out.println("The file already exists, delete it and try again");

                    } else {
                        //write down the new clearedList
                        try {
                            BufferedWriter convertedFile = Files.newBufferedWriter(Paths.get(newFileName));
                            int lineNumber = 1;
                            for (String s : formatList) {
                                if ((s.matches("[0-9]") ||
                                        (s.matches("[0-9]{2}")) ||
                                        (s.matches("[0-9]{3}")) ||
                                        (s.matches("[0-9]{4}")))) {
                                    s = String.valueOf(lineNumber);
                                    lineNumber = lineNumber + 1;
                                } else if (s.matches("\n")) {
                                    s = s.replaceAll("\n", "");
                                }

                                convertedFile.write(s + "\n");

                            }
                            convertedFile.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                } while (true);
            }
            case "s" -> {
                do {

                    //input file from user
                    Scanner in = new Scanner(System.in);
                    System.out.println("What is the filename? ");
                    input = in.nextLine();

                    //check if there are quotation marks (start end), and removes it.
                    if ((input.charAt(0) == '"') && (input.charAt(input.length() - 1) == '"')) {
                        input = input.substring(1, input.length() - 1);
                    }
                    else if ((input.charAt(0) == '\'') && (input.charAt(input.length() - 2) == '\'') && (input.charAt(input.length()-1) == ' ')) {
                        input = input.substring(1, input.length() - 2);
                    }

                    //copy all lines of the file in a List "lines)
                    ArrayList<String> lines = new ArrayList<>();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(input), StandardCharsets.UTF_8));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            lines.add(line);
                        }
                    } catch (FileNotFoundException e) {
                        System.out.println("File not found");
                        continue;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    ArrayList<String> readyList;
                    readyList = freeSpaces(groupLines(removeEmptyLines(lines)));

                    ArrayList<String> finalList;
                    finalList = rimuoviTrattino2(rimuoviTrattino(eliminoParentesiQuadre(eliminoParentesiTonde(readyList))));

                    ArrayList<String> sentenceList;
                    sentenceList = finalSentenceLines(sentenceCase(finalList));

                    ArrayList<String> nameList;
                    nameList = capitalNames(sentenceList);

                    ArrayList<String> formatList;
                    formatList = formatStrings(adjustNewLines(nameList));

                    //Set new name for output converted file
                    String newFileName;
                    newFileName = input + "_cleared.srt";

                    //create a new file and check if the name already exists
                    File f = new File(newFileName);
                    if (f.exists() && !f.isDirectory()) {
                        System.out.println("The file already exists, delete it and try again");

                    } else {
                        //write down the new clearedList
                        try {
                            BufferedWriter convertedFile = Files.newBufferedWriter(Paths.get(newFileName));
                            int lineNumber = 1;
                            for (String s : formatList) {
                                if ((s.matches("[0-9]") ||
                                        (s.matches("[0-9]{2}")) ||
                                        (s.matches("[0-9]{3}")) ||
                                        (s.matches("[0-9]{4}")))) {
                                    s = String.valueOf(lineNumber);
                                    lineNumber = lineNumber + 1;
                                } else if (s.matches("\n")) {
                                    s = s.replaceAll("\n", "");
                                }

                                convertedFile.write(s + "\n");

                            }
                            convertedFile.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                } while (true);
            }
        }

    }



    //elimino tutte le righe vuote
    public static ArrayList<String> removeEmptyLines(ArrayList<String> list) {
        ArrayList<String> toDelete = new ArrayList<>();
        for (String s2 : list) {
            if (s2.isEmpty()) {
                toDelete.add(s2);
            }
        }
        list.removeAll(toDelete);
        return list;
    }

    //salvo in una nuova lista le righe raggruppandole in modo che ogni blocco di sottotitolo
    //venga salvato in un'unica stringa
    //quindi ogni stringa comprenderà 3-4-5 righe a seconda della lunghezza del sottotitolo
    public static ArrayList<String> groupLines(ArrayList<String> list) {
        ArrayList<String> linesGroup = new ArrayList<>();
        String newLine;
        int index = 0;
        int countLines = 1;
        while (list.size() > index) {
            if ((list.get(index).matches("[0-9]") ||
                    (list.get(index).matches("[0-9]{2}")) ||
                    (list.get(index).matches("[0-9]{3}")) ||
                    (list.get(index).matches("[0-9]{4}")))) {
                switch (countLines) {
                    case 3 -> {
                        newLine = list.get(index - 2) + "\n" + list.get(index - 1) + "\n\n";
                        linesGroup.add(newLine);
                        countLines = 2;
                    }
                    case 4 -> {
                        newLine = list.get(index - 3) + "\n" + list.get(index - 2) + "\n" + list.get(index - 1) + "\n\n";
                        linesGroup.add(newLine);
                        countLines = 2;
                    }
                    case 5 -> {
                        newLine = list.get(index - 4) + "\n" + list.get(index - 3) + "\n" + list.get(index - 2) + "\n" + list.get(index - 1) + "\n\n";
                        linesGroup.add(newLine);
                        countLines = 2;
                    }
                    case 6 -> {
                        newLine = list.get(index - 5) + "\n" + list.get(index - 4) + "\n" + list.get(index - 3) + "\n" + list.get(index - 2) + "\n" + list.get(index - 1) + "\n\n";
                        linesGroup.add(newLine);
                        countLines = 2;
                    }
                    default -> countLines++;
                }

            } else {
                countLines++;
            }
            index++;
        }
        return linesGroup;
    }

    //Elimino evevntuali spazi prima e dopo i "newline"
    //I gruppi di stringhe verranno separati da 2 newline
    public static ArrayList<String> freeSpaces(ArrayList<String> list) {
        ArrayList<String> linesNoSpaces = new ArrayList<>();
        Pattern newline = Pattern.compile(".\\n\\n");
        for (String s : list) {
            Matcher matcher = newline.matcher(s);
            if (matcher.find()) {
                String temp = s.replaceAll("\\s\\n", "\n");
                String temp2 = temp.replaceAll("\\n\\s", "\n");
                linesNoSpaces.add(temp2 + "\n");
            } else {

                String temp = s.replaceAll("\\s\\n", "\n");
                String temp2 = temp.replaceAll("\\n\\s", "\n");
                linesNoSpaces.add(temp2);
            }
        }
        return linesNoSpaces;
    }

    //elimino tutte le parentesi tonde e il loro contenuto
    public static ArrayList<String> eliminoParentesiTonde(ArrayList<String> list) {
        Pattern parentesi = Pattern.compile("\\((.*?)\\)\\s");
        Pattern parentesi2 = Pattern.compile("\\((.*?)\\)");
        Pattern parentesi3 = Pattern.compile("\\((.*?)\\n(.*?)\\)");
        ArrayList<String> linesParentesi = new ArrayList<>();
        for (String s : list) {
            Matcher matcher = parentesi.matcher(s);
            Matcher matcher2 = parentesi2.matcher(s);
            Matcher matcher3 = parentesi3.matcher(s);
            if (matcher.find()) {
                String fixed = matcher.replaceAll("");
                linesParentesi.add(fixed);

            } else if (matcher2.find()) {
                String fixed2 = matcher2.replaceAll("");
                linesParentesi.add(fixed2);
            } else if (matcher3.find()) {
                String fixed3 = matcher3.replaceAll("");
                linesParentesi.add(fixed3);
            } else {
                linesParentesi.add(s);
            }
        }
        return linesParentesi;
    }

    //elimino tutte le parentesi quadre e il loro contenuto
    public static ArrayList<String> eliminoParentesiQuadre(ArrayList<String> list) {
        Pattern parentesi = Pattern.compile("\\[(.*?)\\]\\s");
        Pattern parentesi2 = Pattern.compile("\\[(.*?)\\]");
        Pattern parentesi3 = Pattern.compile("\\[(.*?)\\n(.*?)\\]");
        ArrayList<String> linesParentesi = new ArrayList<>();
        for (String s : list) {
            Matcher matcher = parentesi.matcher(s);
            Matcher matcher2 = parentesi2.matcher(s);
            Matcher matcher3 = parentesi3.matcher(s);
            if (matcher.find()) {
                String fixed = matcher.replaceAll("");
                linesParentesi.add(fixed);

            } else if (matcher2.find()) {
                String fixed2 = matcher2.replaceAll("");
                linesParentesi.add(fixed2);
            } else if (matcher3.find()) {
                String fixed3 = matcher3.replaceAll("");
                linesParentesi.add(fixed3);
            } else {
                linesParentesi.add(s);
            }
        }
        return linesParentesi;
    }

    //rimozione del trattino nei seguenti casi
    public static ArrayList<String> rimuoviTrattino(ArrayList<String> list) {
        ArrayList<String> linesTrattino = new ArrayList<>();

      /*  1437
        01:29:11,096 --> 01:29:13,891
       --    */
        Pattern trattini2 = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{3}\\n--\\n");
        Pattern trattini2space = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{3}\\n-\\s-\\s\\n");

    /*833
    01:15:21,120 --> 01:15:23,560
    --I'll get it!*/
        Pattern trattini = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{3}\\n--(.*?)\\n\\n");

    /*1
    00:00:00.000 --> 00:00:04.990
    -(THEME SONG) I'VE GOT A SPECIAL
    POWER, THAT I'M NOT*/
        Pattern trattino1 = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{3}\\n-(.*?)\\n[a-z](.*?)\\n\\n", Pattern.CASE_INSENSITIVE);

    /*13
    00:01:05.090 --> 00:01:08.030
    -HELLO?
    */
        Pattern trattino2 = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{3}\\n-(.*?)\\n\\n");

        for (String s : list) {

            Matcher matchTratto = trattino1.matcher(s);
            Matcher matchTratto2 = trattino2.matcher(s);
            Matcher matchTrattini = trattini.matcher(s);
            Matcher matchTrattini2 = trattini2.matcher(s);
            Matcher matchTrattini2spaces = trattini2space.matcher(s);
            if (matchTrattini.find()) {
                String fixed;
                String[] subString = s.split("\\n--");
                fixed = subString[0] + "\n" + subString[1];
                linesTrattino.add(fixed);
            } else if (matchTratto.find()) {
                String fixed;
                String[] subString = s.split("\\n-");
                fixed = subString[0] + "\n" + subString[1];
                linesTrattino.add(fixed);
            } else if (matchTratto2.find()) {
                String fixed;
                String[] subString = s.split("\\n-");
                fixed = subString[0] + "\n" + subString[1];
                linesTrattino.add(fixed);
            } else if (matchTrattini2.find()) {
                String[] subString = s.split("\\n--");
                linesTrattino.add(subString[0] + "\n\n");
            } else if (matchTrattini2spaces.find()) {
                String[] subString = s.split("-\\s-\\s");
                linesTrattino.add(subString[0] + "\n\n");
            }

            else {
                linesTrattino.add(s);
            }
        }
        return linesTrattino;
    }


    //rimozione trattino caso
           /* 170
            00:11:36,880 --> 00:11:39,840
            -They sit in desks.
            -*/
    public static ArrayList<String> rimuoviTrattino2(ArrayList<String> list) {
        ArrayList<String> linesTrattino2 = new ArrayList<>();
        Pattern trattino3 = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{3}\\n-(.*?)\\n-\\n");
        Pattern trattino4 = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{3}\\n-\\s(.*?)\\n-\\s\\n");
        Pattern trattino5 = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{3}\\n-\\s(.*?)\\n[a-z](.*?)\\n-\\s\\n", Pattern.CASE_INSENSITIVE);
        for (String s : list) {
            Matcher matchTratto3 = trattino3.matcher(s);
            Matcher matchTratto4 = trattino4.matcher(s);
            Matcher matchTratto5 = trattino5.matcher(s);
            if (matchTratto4.find()) {
                String fixed;
                String[] subString = s.split("\\n-\\s");
                fixed = subString[0] + "\n" +subString[1] +"\n\n";
                linesTrattino2.add(fixed);
            } else if (matchTratto5.find()) {
                String fixed;
                String[] subString = s.split("\\n-\\s");
                fixed = subString[0] + "\n" +subString[1] +"\n\n";
                linesTrattino2.add(fixed);
            }
            else if (matchTratto3.find()) {
                String fixed;
                String[] subString = s.split("\\n-");
                fixed = subString[0] + "\n" + subString[1] + "\n\n";
                linesTrattino2.add(fixed);
            }
            else {
                linesTrattino2.add(s);
            }
        }

        return linesTrattino2;
    }



    //Rende maiuscola la prima lettera di ogni sottotitolo
    // e anche le parole come I, I'm ecc...
    public static ArrayList<String> sentenceCase(ArrayList<String> list) {
        Pattern numOne = Pattern.compile("\\d\\n\\d{2}:\\d{2}:\\d{2}");
        Pattern numTwo = Pattern.compile("\\d{2}\\n\\d{2}:\\d{2}:\\d{2}");
        Pattern numThree = Pattern.compile("\\d{3}\\n\\d{2}:\\d{2}:\\d{2}");
        Pattern numFour = Pattern.compile("\\d{4}\\n\\d{2}:\\d{2}:\\d{2}");
        ArrayList<String> lowerCaseLines = new ArrayList<>();
        ArrayList<String> sentenceCaseLines = new ArrayList<>();
        String lower;
        String sentence;
        for (String s : list) {
            Matcher matchNumOne = numOne.matcher(s);
            Matcher matchNumTwo = numTwo.matcher(s);
            Matcher matchNumThree = numThree.matcher(s);
            Matcher matchNumFour = numFour.matcher(s);

            if (matchNumFour.find()) {
                lower = s.substring(0, 36) + s.substring(36).toLowerCase();
                lowerCaseLines.add(lower);
            } else if (matchNumThree.find()) {
                lower = s.substring(0, 35) + s.substring(35).toLowerCase();
                lowerCaseLines.add(lower);
            } else if (matchNumTwo.find()) {
                lower = s.substring(0, 34) + s.substring(34).toLowerCase();
                lowerCaseLines.add(lower);
            } else if (matchNumOne.find()) {
                lower = s.substring(0, 33) + s.substring(33).toLowerCase();
                lowerCaseLines.add(lower);
            }
        }

        for (String s : lowerCaseLines) {
            if ((s.contains(" i ")) ||
                    (s.contains(" i!")) ||
                    (s.contains("i've")) ||
                    (s.contains("i'll")) ||
                    (s.contains("i'm")) ||
                    (s.contains("i'd")) ||
                    (s.contains(" i\\.")) ||
                    (s.contains(" anna")) ||
                    (s.contains("\ni"))) {
                sentence = s.replaceAll(" i ", " I ").replaceAll("i've", "I've")
                        .replaceAll("i'll", "I'll").replaceAll("i'm", "I'm")
                        .replaceAll("i'd", "I'd").replaceAll("\ni ", "\nI ").replaceAll(" i\\.", " I\\.")
                        .replaceAll(" anna", " Anna").replaceAll(" i!", " I!");
                sentenceCaseLines.add(sentence);
            } else {
                sentenceCaseLines.add(s);
            }
        }
        return sentenceCaseLines;
    }

    //rende maiuscola la prima lettera dopo i segni di punteggiatura . ? !
    public static ArrayList<String> finalSentenceLines(ArrayList<String> list) {
        ArrayList<String> finalSentence = new ArrayList<>();
        for (String s : list) {
            if (s.contains(".")||(s.contains("?")||(s.contains("!")))) {
                char[] charArray = new char[s.length()];
                for (int i = 0; i < s.length(); i++) {
                    charArray[i] = s.charAt(i);
                }

                for (int i = 0; i < charArray.length - 2; i++) {
                    if (((charArray[i] == '.') || (charArray[i] == '?') || (charArray[i] == '!')) &&
                            ((charArray[i + 1] == ' ') || (charArray[i + 1] == '\n'))) {
                        charArray[i + 2] = Character.toUpperCase(charArray[i + 2]);
                    } else if (charArray[i] == '.') {
                        charArray[i + 1] = Character.toUpperCase(charArray[i + 1]);
                    }
                }
                String result = new String(charArray);
                finalSentence.add(result);
            } else {
                finalSentence.add(s);
            }

        }
        return finalSentence;
    }

    //Converte i nomi propri presenti nella classe di Enumerazione con la lettera maiuscola
    public static ArrayList<String> capitalNames (ArrayList<String> list) {
        ArrayList<String> capitalNamesLines = new ArrayList<>();
        HashMap<String, String> names = new HashMap<>();
        String nameFixed = "";
        for (Names n : Names.values()) {
            names.put(n.toString().toLowerCase(), String.valueOf(n));
        }
        for (String s : list) {
            for (Map.Entry<String, String> name : names.entrySet()) {
                if (s.contains(name.getKey())) {
                    nameFixed = s.replaceAll(name.getKey(), name.getValue());
                    break;
                } else {
                    nameFixed = s;
                }
            }
            capitalNamesLines.add(nameFixed);
        }
        return capitalNamesLines;
    }

    //elimina il doppio (o triplo) spazio tra un sottotiolo e l'altro e lo sostituisce con uno solo
    public static ArrayList<String> formatStrings (ArrayList<String> list) {
        ArrayList<String> formattedLines = new ArrayList<>();
        for (String s : list) {
            if(s.contains("\n\n\n")) {
                String temp = s.replace("\n\n\n", "\n");
                formattedLines.add(temp);
            }
            else if(s.contains("\n\n")) {
                String temp = s.replace("\n\n", "\n");
                formattedLines.add(temp);
            }

        }
        return formattedLines;
    }

    //Quando si trova nella situazione in cui c'è un "punto" e subito dopo la battuta di un
    //diverso personaggio (senza spazio), questa funzione manda a capo la nuova battuta
    public static ArrayList<String> adjustNewLines (ArrayList<String> list) {
        ArrayList<String> adjustNewLines = new ArrayList<>();
        Pattern dot = Pattern.compile("\\w\\.[A-Z]");
        Pattern questionMark = Pattern.compile("\\w\\?[A-Z]");
        Pattern esclamationMark = Pattern.compile("\\w\\![A-Z]");
        for (String s : list) {
            Matcher matcherDot = dot.matcher(s);
            Matcher matcherQuestion = questionMark.matcher(s);
            Matcher matcherEsclamation = esclamationMark.matcher(s);
            if (matcherDot.find()){
                String temp = s.replaceAll("\\.", "\\.\n");
                adjustNewLines.add(temp);
            } else if (matcherQuestion.find()){
                String temp = s.replaceAll("\\?", "\\?\n");
                adjustNewLines.add(temp);
            } else if (matcherEsclamation.find()){
                String temp = s.replaceAll("\\!", "\\!\n");
                adjustNewLines.add(temp);
            }
            else {
                adjustNewLines.add(s);
            }
        }
        return adjustNewLines;
    }
}



